# ADSB-Fuzzer

Creates the file to be transmitted:
python2 ADSB_Encoder.py  0x4840D6 12.34 56.78 9999.0

Changes the format to 256K:
dd if=Samples.iq8s of=Samples_256K_ONLY1.iq8s bs=4k seek=63

Transmits the file on the HackRF One at the frequency "-f":
hackrf_transfer -t Samples_256K_OG.iq8s -f 433000000 -s 2000000 -x 10
